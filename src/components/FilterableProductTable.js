import React,  { useState } from 'react';
import SearchBar from './SearchBar.js'
import ProductTable from './ProductTable.js'

export default function FilterableProductTable (props) {
  const [filterText, setFilterText] = useState('');
  const [inStockOnly, setInStockOnly] = useState(false);

  return (
      <div>
        <SearchBar
          filterText={filterText}
          inStockOnly={inStockOnly}
          onFilterTextChange={(filterText) => setFilterText(filterText)}
          onInStockChange={(inStockOnly) => setInStockOnly(inStockOnly)}
        />
        <ProductTable
          products={props.products}
          filterText={filterText}
          inStockOnly={inStockOnly}
        />
      </div>
    );
  
}